# Triangle Types Calculator

The sample relies on Angular 6 Framework and the Angular CLI to build the application .

## Demo

https://annashand10.gitlab.io/tradeshiftchallenge/

## Developing Concepts Covered

* Using Tradeshift UI components http://ui.tradeshift.com/
* TypeScript version that relies on classes and modules
* Defining routes including child routes
* Using Custom Components including custom input and output properties
* Defining Properties and Using Events in Components/Directives
* Using BEM methodology as naming convention for classes in HTML and CSS
* Using Angular data-binding Syntax [], () and [()]
* Using reactive forms functionality for capturing and validating data
* Optional: Webpack functionality is available for module loading and more.
* Optional: JIT compilation is available for a production build of the project.
* Using Karma and Jasmine for Unit Testing

### Getting Started
1. `nvm use` - installing needed version of Node.js
2. `npm install -g @angular/cli@6.1.0` - installing Angular CLI globally
3. `npm install`
4. `ng serve`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
