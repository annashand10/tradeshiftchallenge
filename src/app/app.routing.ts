import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppConfig} from './configs/app.config';
import {Error404PageComponent} from './shared/pages/error404-page/error404-page.component';
import {TrianglesComponent} from './triangles/triangles.component';

const appRoutes: Routes = [
    {path: '', component: TrianglesComponent, pathMatch: 'full'},
    {path: AppConfig.routes.error404, component: Error404PageComponent},

    // otherwise redirect to 404
    {path: '**', redirectTo: '/' + AppConfig.routes.error404}
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes, {
        scrollPositionRestoration: 'enabled',
        anchorScrolling: 'enabled'
    })],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
