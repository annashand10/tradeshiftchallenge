import {InjectionToken} from '@angular/core';

export let APP_CONFIG = new InjectionToken('app.config');

export const AppConfig: any = {
    routes: {
        triangles: 'triangles',
        error404: '404'
    },
};
