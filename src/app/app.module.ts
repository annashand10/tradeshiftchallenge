import {APP_BASE_HREF} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app.routing';
import {APP_CONFIG, AppConfig} from './configs/app.config';
import {CoreModule} from './core/core.module';
import {SharedModule} from './shared/shared.module';
import {TrianglesModule} from './triangles/triangles.module';

@NgModule({
    imports: [
        BrowserModule,
        RouterModule,
        AppRoutingModule,
        CoreModule,
        SharedModule,
        TrianglesModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        AppComponent
    ],
    exports: [],
    providers: [
        {provide: APP_CONFIG, useValue: AppConfig},
        {provide: APP_BASE_HREF, useValue: '/tradeshiftchallenge'}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
