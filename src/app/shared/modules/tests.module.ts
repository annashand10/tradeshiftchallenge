import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';

@NgModule({
    exports: [
        BrowserModule,
        HttpClientModule,
        RouterModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    providers: []
})

export class TestsModule {
}
