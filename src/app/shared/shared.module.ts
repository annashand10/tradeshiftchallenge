import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {HeaderComponent} from './components/header/header.component';
import {Error404PageComponent} from './pages/error404-page/error404-page.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
    ],
    declarations: [
        HeaderComponent,
        Error404PageComponent
    ],
    exports: [
        CommonModule,
        HeaderComponent,
        Error404PageComponent
    ]
})

export class SharedModule {
}
