import {async, TestBed} from '@angular/core/testing';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {APP_CONFIG, AppConfig} from '../../../configs/app.config';
import {TestsModule} from '../../modules/tests.module';
import {Error404PageComponent} from './error404-page.component';

describe('Error404Page', () => {
  let fixture;
  let component;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TestsModule,
      ],
      declarations: [
        Error404PageComponent
      ],
      providers: [
        {provide: APP_CONFIG, useValue: AppConfig},
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();

    fixture = TestBed.createComponent(Error404PageComponent);
    fixture.detectChanges();
    component = fixture.debugElement.componentInstance;
  }));

  it('should create Error404Page component', (() => {
    expect(component).toBeTruthy();
  }));
});
