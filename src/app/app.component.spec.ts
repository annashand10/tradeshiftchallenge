import {APP_BASE_HREF} from '@angular/common';
import {TestBed, async} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app.routing';
import {APP_CONFIG, AppConfig} from './configs/app.config';
import {CoreModule} from './core/core.module';
import {SharedModule} from './shared/shared.module';
import {TrianglesModule} from './triangles/triangles.module';

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent
            ],
            imports: [
                BrowserModule,
                RouterModule,
                AppRoutingModule,
                CoreModule,
                SharedModule,
                TrianglesModule,
                FormsModule,
                ReactiveFormsModule,
            ],
            providers: [
                {provide: APP_CONFIG, useValue: AppConfig},
                {provide: APP_BASE_HREF, useValue: '/'}
            ],
        }).compileComponents();
    }));
    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
});
