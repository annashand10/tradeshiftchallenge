import {Component, Input} from '@angular/core';

export enum TriangleSides {
    A = 'a',
    B = 'b',
    C = 'c',
}

@Component({
    selector: 'app-triangles-sample',
    templateUrl: './triangles-sample.component.html',
    styleUrls: ['./triangles-sample.component.scss']
})
export class TrianglesSampleComponent {
    @Input() side: TriangleSides;
}
