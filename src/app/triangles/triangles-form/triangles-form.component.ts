import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {TriangleSides} from '../triangles-sample/triangles-sample.component';
import {calculateTriangleType} from './calculator';

export enum ErrorTypes {
    impossible = 'TRIANGLE-IMPOSSIBLE',
}

interface FormError {
    type: string;
    text: string;
    description: string;
}

@Component({
    selector: 'app-triangles-form',
    templateUrl: './triangles-form.component.html',
    styleUrls: ['./triangles-form.component.scss']
})
export class TrianglesFormComponent implements OnInit, OnChanges {
    @Input() reset: boolean;

    @Output() formSubmitted = new EventEmitter();
    @Output() inputFocus = new EventEmitter();

    public triangleForm: FormGroup;
    public triangleSides = TriangleSides;
    public errors: Array<FormError> = [];
    private hideTimeout: any;

    ngOnInit() {
        this.createForm();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes) {
            this.createForm();
        }
    }

    createForm() {
        const controlConfig = {
            firstSide: new FormControl(null, Validators.required),
            secondSide: new FormControl(null, Validators.required),
            thirdSide: new FormControl(null, Validators.required),
        };

        this.triangleForm = new FormGroup(controlConfig);
    }

    isFieldInvalid(field: AbstractControl): boolean {
        return (field.touched || field.dirty) && field.invalid;
    }

    onInputFocus(side: TriangleSides) {
        this.inputFocus.emit(side);
        this.clearFormErrors();
    }

    onInputFocusOut() {
        this.inputFocus.emit(null);
    }

    addErrorAutoHideTimer() {
        this.hideTimeout = setTimeout(() => {
            this.clearFormErrors();
        }, 9000);
    }

    clearFormErrors() {
        clearTimeout(this.hideTimeout);
        this.errors = [];
    }

    submitHandler() {
        const triangleType = calculateTriangleType(
            this.triangleForm.controls.firstSide.value,
            this.triangleForm.controls.secondSide.value,
            this.triangleForm.controls.thirdSide.value,
        );

        if (!triangleType) {
            const error: FormError = {
                type: ErrorTypes.impossible,
                text: 'Triangle with inputted sides\' length is impossible.',
                description: 'The Triangle Inequality Theorem states that the sum of any 2 ' +
                    'sides of a triangle must be greater than the measure of the third side.'
            };

            this.addErrorAutoHideTimer();
            this.errors.push(error);
        }

        this.formSubmitted.emit({triangleType});
    }
}
