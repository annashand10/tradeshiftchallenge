import {TriangleTypes} from '../triangles-calculator/triangles-calculator.component';

import {calculateTriangleType} from './calculator';

describe('CalculateTriangleType', () => {
    it('should return null if all sides is equal to 0', () => {
        const sideA = 0, sideB = 0, sideC = 0;

        const result = calculateTriangleType(sideA, sideB, sideC);
        expect(result).toBeNull();
    });

    it('should return null if all sides have negative value', () => {
        const sideA = -34, sideB = -1, sideC = -123;

        const result = calculateTriangleType(sideA, sideB, sideC);
        expect(result).toBeNull();
    });

    it('should return null if even one side has negative value', () => {
        const sideA = 34, sideB = -1, sideC = 123;

        const result = calculateTriangleType(sideA, sideB, sideC);
        expect(result).toBeNull();
    });

    it('should return Equilateral if all sides equal', () => {
        const sideA = 12, sideB = 12, sideC = 12;

        const result = calculateTriangleType(sideA, sideB, sideC);
        expect(result).toBe(TriangleTypes.equilateral);
    });

    it('should return Isosceles if sideA is equal to side B', () => {
        const sideA = 12, sideB = 12, sideC = 7;

        const result = calculateTriangleType(sideA, sideB, sideC);
        expect(result).toBe(TriangleTypes.isosceles);
    });

    it('should return Isosceles if side A is equal to side C', () => {
        const sideA = 8, sideB = 12, sideC = 8;

        const result = calculateTriangleType(sideA, sideB, sideC);
        expect(result).toBe(TriangleTypes.isosceles);
    });

    it('should return Isosceles if side B is equal to side C', () => {
        const sideA = 1345, sideB = 12345, sideC = 12345;

        const result = calculateTriangleType(sideA, sideB, sideC);
        expect(result).toBe(TriangleTypes.isosceles);
    });

    it('should return Scalene if side B !== side C && side B !== A', () => {
        const sideA = 12, sideB = 13, sideC = 14;

        const result = calculateTriangleType(sideA, sideB, sideC);
        expect(result).toBe(TriangleTypes.scalene);
    });

    it('should return Scalene if side A !== side C && side A !== B', () => {
        const sideA = 123, sideB = 140, sideC = 130;

        const result = calculateTriangleType(sideA, sideB, sideC);
        expect(result).toBe(TriangleTypes.scalene);
    });

    it('should return Scalene if side C !== side A && side C !== B', () => {
        const sideA = 854, sideB = 654, sideC = 754;

        const result = calculateTriangleType(sideA, sideB, sideC);
        expect(result).toBe(TriangleTypes.scalene);
    });

    it('should return null if sum of side A & side B < side C', () => {
        const sideA = 854, sideB = 654, sideC = 15678;

        const result = calculateTriangleType(sideA, sideB, sideC);
        expect(result).toBeNull();
    });

    it('should return null if sum of side A & side C < side B', () => {
        const sideA = 854, sideB = 1024, sideC = 12;

        const result = calculateTriangleType(sideA, sideB, sideC);
        expect(result).toBeNull();
    });

    it('should return null if sum of side B & side C < side A', () => {
        const sideA = 1024, sideB = 123, sideC = 321;

        const result = calculateTriangleType(sideA, sideB, sideC);
        expect(result).toBeNull();
    });
});
