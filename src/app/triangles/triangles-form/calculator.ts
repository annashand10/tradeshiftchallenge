import {TriangleTypes} from '../triangles-calculator/triangles-calculator.component';

function isTrianglePossible(sideA: number, sideB: number, sideC: number): boolean {
    const AB = sideA + sideB;
    const BC = sideB + sideC;
    const AC = sideC + sideA;

    return AB > sideC
        && BC > sideA
        && AC > sideB;
}

export function calculateTriangleType(sideA: number, sideB: number, sideC: number): TriangleTypes | null {
    if (!isTrianglePossible(sideA, sideB, sideC)) {
        return null;
    }

    const isEquilateral = sideA === sideB
        && sideA === sideC;

    const isIsosceles = sideA === sideB
        || sideA === sideC
        || sideC === sideB;

    if (isEquilateral) {
        return TriangleTypes.equilateral;
    }

    if (isIsosceles) {
        return TriangleTypes.isosceles;
    }

    return TriangleTypes.scalene;
}
