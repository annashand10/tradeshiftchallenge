import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {TrianglesCalculatorComponent} from './triangles-calculator/triangles-calculator.component';
import {TrianglesFormComponent} from './triangles-form/triangles-form.component';
import {TrianglesSampleComponent} from './triangles-sample/triangles-sample.component';
import {TrianglesComponent} from './triangles.component';

@NgModule({
    imports: [
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        TrianglesComponent,
        TrianglesCalculatorComponent,
        TrianglesFormComponent,
        TrianglesSampleComponent,
    ],
    exports: [
        TrianglesComponent,
        TrianglesCalculatorComponent,
        TrianglesFormComponent,
        TrianglesSampleComponent,
    ],
})

export class TrianglesModule {
}
