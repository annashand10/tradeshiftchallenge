import {Component} from '@angular/core';
import {TriangleSides} from '../triangles-sample/triangles-sample.component';

export enum TriangleTypes {
    equilateral = 'Equilateral',
    isosceles = 'Isosceles',
    scalene = 'Scalene'
}

@Component({
    selector: 'app-triangles-calculator',
    templateUrl: './triangles-calculator.component.html',
    styleUrls: ['./triangles-calculator.component.scss']
})
export class TrianglesCalculatorComponent {
    public triangleType: TriangleTypes;
    private _highlightedSide: TriangleSides = null;
    public resetForm = false;

    public onFormSubmit = (event) => {
        this.triangleType = event.triangleType;
    }

    set highlightedSide(side: TriangleSides) {
        this._highlightedSide = side;
    }

    get highlightedSide(): TriangleSides {
        return this._highlightedSide;
    }

    public onFormInputFocus(side: TriangleSides) {
        this.resetForm = false;
        this.highlightedSide = side;
    }

    reset() {
        this.resetForm = true;
        this.triangleType = null;
    }
}
