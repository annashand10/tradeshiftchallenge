import {browser} from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getAppTitle() {
      browser.get('http://localhost:4200/tradeshiftchallenge/');

      return browser.getTitle();
  }
}
